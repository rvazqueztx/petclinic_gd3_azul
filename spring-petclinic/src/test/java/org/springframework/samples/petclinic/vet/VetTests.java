/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;

import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Dave Syer
 *
 */

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VetTests {
	private Vet vet;
	@Autowired
	private VisitRepository visitrep;
	@Autowired
	private VetRepository vetrep;
	@Autowired
	private PetRepository petrep;
	private Visit visit;
	private Set<Visit> visits;
	private Pet pet;
	private Owner owner;
	@Autowired
	private OwnerRepository ownerrep;
	
	
	@Before
	public void init() {
		if(visits==null) {
			this.visits= new HashSet<Visit>();
		}
		
		if(vet==null) {
			vet = new Vet();
			vet.setFirstName("Miguel");
			vet.setHomeVisits(false);
			vet.setLastName("Paredes");
			vetrep.save(vet);
		 }
		 
		
		if(pet==null) {
			pet=new Pet();
			pet.setBirthDate(LocalDate.of(2008, Month.APRIL, 15));
			pet.setName("Pascasio");
			PetType pettype = new PetType();
			pettype.setId(3);
			pet.setType(pettype);
			
			pet.setWeight(80);
			
		}
		
		if(this.owner == null) {
		      owner = new Owner();
		      owner.setAddress("Avenida Europa");
		      owner.setCity("Cáceres");
		      owner.setFirstName("Paco");
		      owner.setLastName("Martínez");
		      owner.setTelephone("648393002");
		      owner.addPet(pet);
		      this.ownerrep.save(owner);
		      this.petrep.save(pet);
		      
		    }
		
		
		 if (visit==null) {
			 visit=new Visit();
			 visit.setDate(LocalDate.of(2012, Month.JUNE, 25));
			 visit.setDescription("Prueba");
			 visit.setVet(vet); 
			 visit.setPetId(pet.getId());
			 visitrep.save(visit);
		 }
		 
		
	}
	
    @Test
    public void testSerialization() {
        Vet vet = new Vet();
        vet.setFirstName("Zaphod");
        vet.setLastName("Beeblebrox");
        vet.setId(123);
        Vet other = (Vet) SerializationUtils
                .deserialize(SerializationUtils.serialize(vet));
        assertThat(other.getFirstName()).isEqualTo(vet.getFirstName());
        assertThat(other.getLastName()).isEqualTo(vet.getLastName());
        assertThat(other.getId()).isEqualTo(vet.getId());
    }
    
    @Test
	public void testVet() {
		assertNotNull(vet);				
	}
	
	
	@Test
	public void testHomeVisits() {
		assertNotNull(vet.getHomeVisits());
		assertFalse(vet.getHomeVisits());
		vet.setHomeVisits(true);
		assertTrue(vet.getHomeVisits());
	}

    @Test
    public void getVisitsInternal() {
    	visits.add(visit);
		vet.setVisitsInternal(visits);
    	assertNotNull(vet.getVisitsInternal());
		assertEquals(vet.getVisitsInternal(), this.visits);
		assertEquals(vet.getVisitsInternal(), this.visits);
		assertEquals(vet.getVisitsInternal().size(),1);
    }
    
    @Test
    public void setVisitsInternal() {
    	Set<Visit> visits = new HashSet<Visit>();
    	visits.add(visit);
    	vet.setVisitsInternal(visits);
    	assertNotNull(vet.getVisitsInternal());
		assertEquals(vet.getVisitsInternal(), visits);
    }
    
    @Test
    public void getVisits() {
    	Set<Visit> visits = new HashSet<Visit>();
    	visits.add(visit);
    	vet.setVisitsInternal(visits);
    	assertNotNull(vet.getVisits());
    	assertEquals(vet.getVisits().size(),1);
    }
    
    @Test
    public void addVisit() {
    	Set<Visit> visits = new HashSet<Visit>();
    	Visit v= new Visit();
    	Visit v1= new Visit();
    	visits.add(v);
    	vet.setVisitsInternal(visits);
    	assertNotNull(vet.getVisitsInternal());
    	assertEquals(vet.getVisitsInternal().size(), 1);
    	vet.addVisit(v1);
		assertEquals(vet.getVisitsInternal().size(), 2);
    }
}
