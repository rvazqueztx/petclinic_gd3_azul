package org.springframework.samples.petclinic.owner;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTests {
	@Autowired
	private PetRepository petrep;

	private Pet pet;
	private Owner owner;
	@Autowired
	private OwnerRepository ownerrep;
	
	@Before
	public void init() {
		
		
		if(pet==null) {
			pet=new Pet();
			pet.setBirthDate(LocalDate.of(2008, Month.APRIL, 15));
			pet.setName("Valiant");
			PetType pettype = new PetType();
			pettype.setId(3);
			pet.setType(pettype);
			pet.setWeight(80);
			pet.setComments("no muerde");
			
			
		}
		
		if(this.owner == null) {
		      owner = new Owner();
		      owner.setAddress("Avenida Europa");
		      owner.setCity("Cáceres");
		      owner.setFirstName("Raúl");
		      owner.setLastName("Martínez");
		      owner.setTelephone("648393002");
		      owner.addPet(pet);
		      this.ownerrep.save(owner);
		      this.petrep.save(pet);
		      
		    }
		
		 
	}

	
	@Test
	public void findPetsByNameTest() {
		
		assertNotNull(petrep.findPetsByName(pet.getName()));
		
		Collection <Pet> findPet = this.petrep.findPetsByName(pet.getName());
		if (!findPet.isEmpty()) {				
			for (Pet petF : findPet) {
				System.out.println(pet.getName());
				assertEquals(petF.getName(), this.pet.getName() ) ;
			}				
		}
		
		
		
		
	}
	
	
	@Test
	public void delete() {		
		assertNotNull(petrep.findById(this.pet.getId()));
		petrep.deletePetInfo(this.pet.getId());
		petrep.delete(this.pet);
        assertNull(petrep.findById(this.pet.getId()));
	}
	
	
	
	

}
