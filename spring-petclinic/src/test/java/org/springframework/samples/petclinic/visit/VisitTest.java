package org.springframework.samples.petclinic.visit;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitTest {
	
	
	private Vet vet;
	@Autowired
	private VisitRepository visitrep;
	@Autowired
	private VetRepository vetrep;
	@Autowired
	private PetRepository petrep;
	private Visit visit;
	private Pet pet;
	private Owner owner;
	@Autowired
	private OwnerRepository ownerrep;
	
	@Before
	public void init() {
		
		if(vet==null) {
			vet = new Vet();
			vet.setFirstName("Fernandez");
			vet.setHomeVisits(false);
			vet.setLastName("Carrasco");
			 vetrep.save(vet);
		 }
		 
		
		if(pet==null) {
			pet=new Pet();
			pet.setBirthDate(LocalDate.of(2008, Month.APRIL, 15));
			pet.setName("Pascasio");
			PetType pettype = new PetType();
			pettype.setId(3);
			pet.setType(pettype);
			
			pet.setWeight(80);
			
		}
		
		if(this.owner == null) {
		      owner = new Owner();
		      owner.setAddress("Avenida Europa");
		      owner.setCity("Cáceres");
		      owner.setFirstName("Paco");
		      owner.setLastName("Martínez");
		      owner.setTelephone("648393002");
		      owner.addPet(pet);
		      this.ownerrep.save(owner);
		      this.petrep.save(pet);
		      
		    }
		
		
		 if (visit==null) {
			 visit=new Visit();
			 visit.setDate(LocalDate.of(2012, Month.JUNE, 25));
			 visit.setDescription("Prueba");
			 visit.setVet(vet); 
			 visit.setPetId(pet.getId());
			 visitrep.save(visit);
		 }
		 
	}

	@Test
	public void getVetTest() {
		assertNotNull(visit.getVet());
		assertEquals(visit.getVet(), this.vet);
		assertEquals(visit.getVet().getId(),this.vet.getId());
	}
	
	@Test
	public void setVetTest() {
		Vet vet2 = new Vet();
		visit.setVet(vet2);
		assertNotNull(visit.getVet());
		assertEquals(visit.getVet(), vet2);
		assertEquals(visit.getVet().getId(), vet2.getId());
		
	}


}
